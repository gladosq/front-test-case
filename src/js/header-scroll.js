const SCROLL_HEIGHT = 20;
const headerWrapper = document.querySelector('.header');

window.addEventListener('scroll', function() {
  if (pageYOffset > SCROLL_HEIGHT) {
    if (!headerWrapper.classList.contains('header--scroll')) {
      headerWrapper.classList.add('header--scroll');
    }

  } else {
    if (headerWrapper.classList.contains('header--scroll')) {
      headerWrapper.classList.remove('header--scroll');

    }
  }
});
