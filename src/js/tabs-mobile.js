const tabsButton = document.querySelector('.tabs__mobile-button');
const tabsList = document.querySelector('.tab-header-wrap');

if (tabsButton) {
    tabsButton.addEventListener('click', function () {
        tabsList.classList.toggle('tab-header-wrap--active');

    });
}
