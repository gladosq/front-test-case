const gulp = require('gulp');
const plumber = require('gulp-plumber');
const sourcemap = require('gulp-sourcemaps');
const sass = require('gulp-sass');
const postcss = require('gulp-postcss');
const autoprefixer = require('autoprefixer');
const csso = require('postcss-csso');
const rename = require('gulp-rename');
const sync = require('browser-sync').create();
const htmlmin = require('gulp-htmlmin');
const imagemin = require('gulp-imagemin');
const webp = require('gulp-webp');
const del = require('del');
const svgstore = require('gulp-svgstore');
const concat = require('gulp-concat');
const babel = require('gulp-babel');

const styles = () => {
  return gulp.src('src/scss/styles.scss')
    .pipe(plumber())
    .pipe(sourcemap.init())
    .pipe(sass())
    .pipe(postcss([
      autoprefixer()
    ]))
    .pipe(gulp.dest('dist/css'))
    .pipe(postcss([
      csso()
      ]))
    .pipe(sourcemap.write('.'))
    .pipe(rename('styles.min.css'))
    .pipe(gulp.dest('dist/css'))
    .pipe(sync.stream());
}

exports.styles = styles;

const html = () => {
  return gulp.src('src/*.html')
    .pipe(gulp.dest('dist'));
}

exports.html = html;

const scripts = () => {
  return gulp.src('src/js/*.js')
    .pipe(babel({
            presets: ['@babel/preset-env']
        }))
    .pipe(concat('main.js'))
    .pipe(gulp.dest("dist/js"))
    .pipe(sync.stream());
}

exports.scripts = scripts;

const images = () => {
  return gulp.src('src/img/*.{jpg,png}')
    .pipe(imagemin([
      imagemin.mozjpeg({progressive: true}),
      imagemin.optipng({optimizationLevel: 3}),
    ]))
    .pipe(gulp.dest('dist/img'));
}

exports.images = images;

const createWebp = () => {
  return gulp.src('src/img/*.{jpg,png}')
    .pipe(webp({quality: 90}))
    .pipe(gulp.dest('dist/img'))
}

exports.createWebp = createWebp;

const sprite = () => {
  return gulp.src("src/img/*.svg")
    .pipe(svgstore())
    .pipe(rename("sprite.svg"))
    .pipe(gulp.dest("dist/img"));
}

exports.sprite = sprite;

const copy = (done) => {
  return gulp.src(
    [
      'src/fonts/*.{woff2,woff}',
      'src/*.ico',
      'src/img/*.{jpg,png,svg}',
    ], {
      base: 'src'
    })
    .pipe(gulp.dest('dist'))
  done();
}

exports.copy = copy;

const vendors = (done) => {
  return gulp.src(
    [
      'src/js/vendors/*.js'
    ], {
      base: 'src/js/vendors'
    })
    .pipe(gulp.dest('dist/js'))
  done();
}

exports.vendors = vendors;

const clean = () => {
  return del('dist');
}

exports.clean = clean;

const server = (done) => {
  sync.init({
    server: {
      baseDir: 'dist'
    },
    cors: true,
    notify: false,
    ui: false,
  });
  done();
}

exports.server = server;

const reload = (done) => {
  sync.reload();
  done();
}

exports.reload = reload;

const watcher = () => {
  gulp.watch('src/scss/**/*.scss', gulp.series(styles));
  gulp.watch("src/js/*.js", gulp.series(scripts));
  gulp.watch('src/*.html', gulp.series(html, reload));
}

exports.watcher = watcher;

const build = gulp.series(clean,
  gulp.parallel(
    styles,
    html,
    scripts,
    vendors,
    copy,
    images,
    createWebp
  )
);

exports.build = build;

exports.default = gulp.series(clean,
  gulp.parallel(
    styles,
    html,
    scripts,
    vendors,
    copy,
    createWebp
  ),
  gulp.series(
    server,
    watcher
  )
);
