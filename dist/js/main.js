"use strict";

var SCROLL_HEIGHT = 20;
var headerWrapper = document.querySelector('.header');
window.addEventListener('scroll', function () {
  if (pageYOffset > SCROLL_HEIGHT) {
    if (!headerWrapper.classList.contains('header--scroll')) {
      headerWrapper.classList.add('header--scroll');
    }
  } else {
    if (headerWrapper.classList.contains('header--scroll')) {
      headerWrapper.classList.remove('header--scroll');
    }
  }
});
"use strict";

var tabsButton = document.querySelector('.tabs__mobile-button');
var tabsList = document.querySelector('.tab-header-wrap');

if (tabsButton) {
  tabsButton.addEventListener('click', function () {
    tabsList.classList.toggle('tab-header-wrap--active');
  });
}
"use strict";

(function ($) {
  'use strict';
  /**
   * Табы
   */

  $.fn.tabs = function () {
    var $self = $(this);
    var $tabHeaders = $self.find('.js-tab-header').filter(function (index, el) {
      return $(el).parentsUntil($self).length === 1;
    });
    var $tabContent = $self.find('.js-tab-content').filter(function (index, el) {
      return $(el).parentsUntil($self).length === 1;
    });
    /**
     * Активация таба по его индексу
     * @param {Number} index - индекс таба, который нужно активировать
     */

    var selectTab = function selectTab(index) {
      $tabHeaders.removeClass('active').eq(index).addClass('active');
      $tabContent.removeClass('active').eq(index).addClass('active');
    };
    /**
     * Инициализаиця
     */


    var init = function init() {
      selectTab(0); // Обработка событий

      $tabHeaders.on('click', function () {
        selectTab($(this).index());
      });
    };

    init();
    this.selectTab = selectTab;
    return this;
  }; // Инициализируем табы на всех блоках с классом 'js-tabs'


  $('.js-tabs').each(function () {
    $(this).data('tabs', $(this).tabs());
  });
})(jQuery);
"use strict";

document.addEventListener('DOMContentLoaded', function () {
  WZoom.create('#myContent', {
    type: 'html',
    width: 300,
    height: 200,
    dragScrollable: true,
    maxScale: 2,
    smoothExtinction: 0.1
  }); // var degreeRotation = 0;
  // function changeTransformRotateProperty(element, value) {
  //     element.style.transform = element.style.transform.replace(/(^|\s)rotate\(\d+deg\)($|\s)/i, '') + ' rotate(' + value + 'deg)';
  // }
  // function rotateImage() {
  //     var animationDuration = 0.2;
  //     image.style.transition = 'transform ' + animationDuration + 's';
  //     changeTransformRotateProperty(image, degreeRotation);
  //     if (degreeRotation > 270 && animationDuration) {
  //         setTimeout(function () {
  //             changeTransformRotateProperty(image, 0);
  //         }, animationDuration * 1000);
  //     }
  // }
  // var observer = new MutationObserver(rotateImage);
  // observer.observe(image, { attributes: true, attributeFilter: [ 'style' ] });
});
"use strict";

$(document).ready(function () {
  $('div.tab-content__image-wrapper').zoom({
    url: 'img/big-image.jpg'
  });
});
$(document).ready(function () {
  $('div.tab-content__image-click-wrapper').zoom({
    on: 'grab',
    url: 'img/big-image-2.jpg'
  });
});